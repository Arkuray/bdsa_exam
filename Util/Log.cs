﻿using System;
using System.Diagnostics;

namespace Util.Server {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    public static class Log {
        public static void Append(string message, EventType e, string address = "") {
            var timestamp = "[" + DateTime.Now + "] ";
            var evt = "[" + e.ToString().PadRight(5) + "] ";
            var messenger = address == "" ? "" : ( "[" + address + "] ");
            var entry =  timestamp  + evt + messenger + message;
            Console.WriteLine(entry);
        }
        
        [Conditional("DEBUG")]
        public static void AppendDebug(string message, EventType e, string address = "") {
            Append(message,e,address);
        }
    }
}
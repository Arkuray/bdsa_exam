﻿using System;
using System.Text;

namespace Util.Client {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    [Serializable]
    public struct PersonData {
        public uint VotercardNumber { get; set; }
        public string Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public uint PostalCode { get; set; }
        public string PostalDistrict { get; set; }
        public bool Empty {
            get {
                return VotercardNumber == 0
                    && Address == null
                    && FirstName == null
                    && LastName == null
                    && City == null
                    && PostalCode == 0
                    && PostalDistrict == null;
            }
        }
        public override string ToString() {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Votercard number: " + VotercardNumber);
            builder.AppendLine("First name: " + FirstName);
            builder.AppendLine("Last name: " + LastName);
            builder.AppendLine("Address: " + Address);
            builder.AppendLine("City: " + City);
            builder.AppendLine("Postal code: " + PostalCode);
            builder.AppendLine("Postal district: " + PostalDistrict);
            return builder.ToString();
        }
    }
}

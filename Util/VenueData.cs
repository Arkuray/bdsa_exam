﻿using System;
using System.Text;

namespace Util.Client {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    [Serializable]
    public struct VenueData {
        public string Address { get; set; }
        public string Name { get; set; }
        public uint PostalCode { get; set; }
        public string PostalDistrict { get; set; }
        public bool Empty {
            get {
                return Address == null
                    && Name == null
                    && PostalCode == 0
                    && PostalDistrict == null;
            }
        }
        public override string ToString() {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Name: " + Name);
            builder.AppendLine("Address: " + Address);
            builder.AppendLine("Postal code: " + PostalCode);
            builder.AppendLine("Postal district: " + PostalDistrict);
            return builder.ToString();
        }
    }
}

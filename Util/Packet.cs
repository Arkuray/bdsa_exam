﻿using System;
namespace Util {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    [Serializable]
    public struct Packet {
        public uint ID { get; set; }
        public Command Command { get; set; }
        public object Content { get; set; }
    }
}

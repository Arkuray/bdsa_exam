﻿namespace Util.Server {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    public enum EventType {
        Event,
        From,
        To
    }
}

﻿using System;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace Util {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public class Message {
        private static readonly ICryptoTransform encryptor, decryptor;

        private readonly Packet _packet;
        public uint ID { get { return _packet.ID; } }
        public Command Command { get { return _packet.Command; } }
        public object Content { get { return _packet.Content; } }
        public IPEndPoint Address { get; private set; }

        static Message() {
            byte[] key = HexStringToByteArray(ConfigurationManager.AppSettings["EncryptionKey"]);
            byte[] iv = HexStringToByteArray(ConfigurationManager.AppSettings["EncryptionVector"]);
            AesManaged aes = new AesManaged();
            encryptor = aes.CreateEncryptor(key, iv);
            decryptor = aes.CreateDecryptor(key, iv);
        }

        public Message(Command command, object content, IPEndPoint address, uint id = 0) {
            _packet.ID = id;
            _packet.Command = command;
            _packet.Content = content;
            Address = address;
        }

        [Pure]
        public byte[] PreparePacket() {
            Contract.Ensures(Message.ExtractMessage(Contract.Result<byte[]>(),Address).Equals(this));
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, _packet);
            return Encrypt(stream.ToArray()).ToArray();
        }

        [Pure]
        public static Message ExtractMessage(byte[] packet, IPEndPoint sender) {
            MemoryStream stream = Decrypt(packet);
            stream.Position = 0;
            BinaryFormatter formatter = new BinaryFormatter();
            Packet p = (Packet)formatter.Deserialize(stream);
            return new Message(p.Command, p.Content, sender, p.ID);
        }

        [Pure]
        private static MemoryStream Encrypt(byte[] data) {
            Contract.Ensures(Decrypt(Contract.Result<MemoryStream>().ToArray())
                .ToArray().SequenceEqual(data));
            MemoryStream stream = new MemoryStream();
            CryptoStream cs = new CryptoStream(stream, encryptor, CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();
            return stream;
        }

        [Pure]
        private static MemoryStream Decrypt(byte[] data) {
            MemoryStream stream = new MemoryStream();
            CryptoStream cs = new CryptoStream(stream, decryptor, CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();
            return stream;
        }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;

            Message that = (Message) obj;
            bool result = false;
            result |= that.Address.Equals(this.Address);
            result |= that.Content.Equals(this.Content);
            result |= that.Command.Equals(this.Command);

            return result;
        }

        public override int GetHashCode() {
            return (int)ID | Command.GetHashCode() | Content.GetHashCode() | Address.GetHashCode();
        }

        [Pure]
        private static byte[] HexStringToByteArray(string hex) {
            Contract.Requires(hex != null);
            Contract.Requires(hex.Length % 2 == 0);
            Contract.Requires(Contract.ForAll(hex,
                x => char.IsNumber(x) || (x >= 'a' && x <= 'f') || (x >= 'A' && x <= 'F')));
            return
                (from x in Enumerable.Range(0, hex.Length)
                 where x % 2 == 0
                 select Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }
    }
}

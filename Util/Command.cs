﻿namespace Util {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public enum Command {
        RegisterVoter,
	    BackupRegistration,
	    BackupAcknowledged,
	    AlreadyRegistered,
	    RegistrationDone,
	    WrongVenue,
	    MakeDatabase,
	    GiveMeSnapshot,
	    PrimaryNotResponding,
	    AreYouAlive,
	    AmAlive,
	    Candidate,
	    InterruptElection,
	    StepDown
    };
}
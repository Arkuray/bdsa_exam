﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Diagnostics.Contracts;

namespace Util {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    public class DatabaseAPI {
        private readonly SQLiteConnection Connection;
        private static Dictionary<string, DatabaseAPI> InstanceMap = new Dictionary<string, DatabaseAPI>(); 
        private List<List<string>> _transactionList;
        private const string StandardPath = "Data Source=|DataDirectory|/../../../lib/VoterDatabase.s3db;New=False;";
        private DatabaseAPI(string connectionString) {
            Connection = new SQLiteConnection(connectionString);
        }
        public static DatabaseAPI GetInstance(string connectionString = StandardPath) {
            Contract.Requires(connectionString != (string)null);
            if(InstanceMap.ContainsKey(connectionString)) {
                return InstanceMap[connectionString];
            } 
            var result = new DatabaseAPI(connectionString);
            InstanceMap.Add(connectionString,result);
            return result;
        }

        public DbDataReader ExecuteReader(string sql) {
            return Execute(sql, GetReader);
        }
        public object ExecuteScalar(string sql) {
            return Execute(sql, cmd => cmd.ExecuteScalar());
        }
        public int ExecuteNonQuery(string sql) {
            return Execute(sql, cmd => cmd.ExecuteNonQuery());
        }
        
        public int ExecuteNonQueryTransaction(string sql, List<List<string>> param) {
            _transactionList = param;
            var rowsChanged = Execute(sql, Transaction);
            _transactionList = null;
            return rowsChanged;
        }

        private int Transaction(SQLiteCommand cmd) {
            var transaction = Connection.BeginTransaction();
            var paramList = _transactionList.Select(param => new SQLiteParameter()).ToList();
            foreach (var sqLiteParameter in paramList) {cmd.Parameters.Add(sqLiteParameter);}
            int rowsChanged = 0;
            for (int i = 0; i < _transactionList[0].Count; i++) {
                for (int j = 0; j < paramList.Count; j++) {
                    paramList[j].Value = _transactionList[j][i];
                }
                rowsChanged += cmd.ExecuteNonQuery();
            }
            transaction.Commit(); 
            return rowsChanged;
        }
        //have to load it into a datatable
        //to fetch all the data before we close connection.
        private DbDataReader GetReader(SQLiteCommand cmd) { 
            var dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            return dt.CreateDataReader();
        }
        private T Execute<T>(string sql, Func<SQLiteCommand, T> f) {
            Connection.Open();
            var cmd = new SQLiteCommand(Connection) {CommandText = sql};
            T result = f.Invoke(cmd);
            Connection.Close();
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Diagnostics.Contracts;
using Util.Client;

namespace Util.Server {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    public static class StatedependantDatabase {
        private static readonly DatabaseAPI db
            = DatabaseAPI.GetInstance(ConfigurationManager.ConnectionStrings["VoterDatabase"].ConnectionString);

        [Pure]
        public static bool IsRegistered(uint voter) {
            var sqlQuery = "SELECT Voted FROM Voter WHERE VoterID = " + voter;
            var result = (long)db.ExecuteScalar(sqlQuery);
            return result == 1;
        }

        public static void RegisterVoter(uint voter) {
            Contract.Requires(StatelessDatabase.Exists(voter));
            Contract.Requires(!IsRegistered(voter));
            Contract.Ensures(IsRegistered(voter));
            var sqlQuery = "UPDATE Voter SET Voted = 1 WHERE VoterID = " + voter;
            db.ExecuteNonQuery(sqlQuery);
        }

        [Pure]
        public static List<List<uint>> GetRegisteredVoterDump() {
            var result = new List<List<uint>>();
            var thereIsMore = true;
            const uint partDumpSize = 5000;
            uint i = 0;
            while(thereIsMore) {
                var partDump = GetRegisteredVoters(i, partDumpSize);
                thereIsMore = partDump.Count == partDumpSize;
                result.Add(partDump);
                i += partDumpSize;
            }
            return result;
        }

        [Pure]
        private static List<uint> GetRegisteredVoters(uint start, uint count) {
            //Contract.Ensures(Contract.ForAll(Contract.Result<List<uint>>(), IsRegistered)); //this simple kills performance to an unacceptable point when working with large datasets
            var sqlQuery = "SELECT VoterID FROM Voter WHERE Voted = 1 LIMIT "+start+","+count;
            var reader = db.ExecuteReader(sqlQuery);
            return ReadRegisteredList(reader);
        }

        [Pure]
        private static List<uint> ReadRegisteredList(DbDataReader reader) {
            var result = new List<uint>();
            while (reader.Read()) 
                result.Add((uint)reader.GetInt64(0));
            return result;
        }

        public static void RegisterDump(List<uint> voters) {
            Contract.Ensures(Contract.ForAll(voters, IsRegistered));
            const string sqlQuery = "UPDATE Voter SET Voted = 1 WHERE VoterID = ?";
            var paramList = new List<List<string>> {voters.ConvertAll(v => v.ToString())};
            db.ExecuteNonQueryTransaction(sqlQuery, paramList); 
        }
    }
}

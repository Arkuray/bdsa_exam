﻿using System;
using System.Collections;
using System.Diagnostics.Contracts;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using com.google.zxing;
using com.google.zxing.common;
using com.google.zxing.qrcode;

namespace Util.Client {
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
    public class VotercardScanner {
        private Capture camera = null;
        public delegate void QRHandler(uint scanResult);
        public event QRHandler QRCodeFound;
        public Hashtable _decodeHints = new Hashtable();

        public VotercardScanner() {
            try {
                camera = new Capture();
            } catch (Exception e) {}
            _decodeHints.Add(DecodeHintType.TRY_HARDER, true);
            camera.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT, 200);
            camera.SetCaptureProperty(CAP_PROP.CV_CAP_PROP_FRAME_WIDTH, 200);
        }

        [Pure]
        public Boolean IsCameraAvaiable() {
            return camera != null;
        }

        //Attach this to Application.idle
        public Image<Gray, byte> ScanCamera() {
            Contract.Requires(IsCameraAvaiable());
            var frame = camera.QueryGrayFrame();
            Reader reader = new QRCodeReader();

            RGBLuminanceSource lum = new RGBLuminanceSource(frame.ToBitmap(), frame.Width, frame.Height);
            HybridBinarizer binarizer = new HybridBinarizer(lum);
            var bitmap = new BinaryBitmap(binarizer);
            try {
                Result res = reader.decode(bitmap, _decodeHints);
                uint number;
                if (QRCodeFound != null & uint.TryParse(res.Text, out number)) QRCodeFound(number);
            } catch (ReaderException re) {
                //No barcode found;
            }
            return frame;
        }
    }
}

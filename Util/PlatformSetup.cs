﻿namespace Util.Client {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public enum PlatformSetup {
        Client,
        Server,
        Combi
    }
}

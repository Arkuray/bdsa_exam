﻿using System;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.IO;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using com.google.zxing;
using com.google.zxing.qrcode;

namespace Util.Client {
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
    public class VotercardBuilder {
        private PdfDocument _document;
        private PdfPage _page;
        private int _voterCardsOnPage = 0;

        private String _electionType = "";
        private DateTime _electionDate = DateTime.Now;
        private String _votingVenue = "Voting venue";
        private String _votingNumber = "Voting number";
        private String _sender = "Sender";
        private String _receiver = "Reciever";
        private String _votingTime = "Time of voting";
        private String _votingTimestamp = "09.00 - 20.00";
        private String _postMark = "postmark.png";

        public void CreateNewDocument(String title = "PDF Document") {
            _document = new PdfDocument();
            _document.Info.Title = title;
        }

        public void SetElectionType(string electionType) {
            _electionType = electionType;
        }

        public void SetElectionDate(DateTime date) {
            _electionDate = date;
        }

        public void WriteVoterCard(PersonData person, VenueData venue) {
            if (IsPageFull()) CreateNewPage();

            var QRencoder = new QRCodeWriter();
            var gfx = XGraphics.FromPdfPage(_page);
            double cardHeight = _page.Height / 4;
            double cardTopOffset = cardHeight * _voterCardsOnPage;

            var pen = new XPen(XColor.FromName("black"), 0.5);
            var titleF = new XFont("Verdana", 13, XFontStyle.Bold);
            var boldF = new XFont("Verdana", 9, XFontStyle.Bold);
            var smallF = new XFont("Verdana", 4, XFontStyle.Italic);
            var normalF = new XFont("Verdana", 8);

            var electionBox = new XRect(25, cardTopOffset + 3, 200, 40);
            var venueBox = new XRect(25, cardTopOffset + 50, 200, 50);
            var voterNumberBox = new XRect(25, cardTopOffset + 110, 200, 20);
            var timesBox = new XRect(25, cardTopOffset + 140, 200, 20);
            var tableBox = new XRect(25, cardTopOffset + 170, 200, 20);
            var senderBox = new XRect(375, cardTopOffset + 90, 100, 40);
            var recieverBox = new XRect(375, cardTopOffset + 130, 200, 100);

            var venueTextBox = new XRect(venueBox.X + 5, venueBox.Y + 5, venueBox.Width, venueBox.Height);
            var voterNumberTextBox = new XRect(voterNumberBox.X + 5, voterNumberBox.Y + 5, voterNumberBox.Width, voterNumberBox.Height);
            var timesTextBox = new XRect(timesBox.X + 5, timesBox.Y + 5, timesBox.Width, timesBox.Height);
            var senderTextBox = new XRect(senderBox.X + 5, senderBox.Y + 5, senderBox.Width, senderBox.Height);
            var recieverTextBox = new XRect(recieverBox.X + 5, recieverBox.Y + 5, recieverBox.Width, recieverBox.Height);

            var formatter = new XTextFormatter(gfx);

            //Write the QRcode first
            int size = 200;
            var QRmatrix = QRencoder.encode(person.VotercardNumber.ToString(), BarcodeFormat.QR_CODE, size, size);

            var img = new Bitmap(size, size);
            for (int y = 0; y < QRmatrix.Height; ++y) {
                for (int x = 0; x < QRmatrix.Width; ++x) {
                    if (QRmatrix.get_Renamed(x, y) == -1) {
                        img.SetPixel(x, y, Color.White);
                    } else {
                        img.SetPixel(x, y, Color.Black);
                    }
                }
            }

            var QRcode = XImage.FromGdiPlusImage(img);
            gfx.DrawImage(QRcode, 200, cardTopOffset + 15, QRcode.PixelWidth, QRcode.PixelWidth);

            //Then write the design
            gfx.DrawRectangle(pen, 0, cardTopOffset, _page.Width, cardHeight);
            gfx.DrawRectangle(pen, venueBox);
            gfx.DrawRectangle(pen, voterNumberBox);
            gfx.DrawRectangle(pen, timesBox);
            gfx.DrawRectangle(pen, tableBox);

            gfx.DrawLine(pen, 375, cardTopOffset + 50, 375, cardTopOffset + cardHeight - 25);
            gfx.DrawLine(pen, 375, cardTopOffset + 90, _page.Width - 50, cardTopOffset + 90);
            gfx.DrawLine(pen, 375, cardTopOffset + 130, _page.Width - 50, cardTopOffset + 130);
            gfx.DrawLine(pen, 375, cardTopOffset + 90, 375 + 100, cardTopOffset + 130);
            gfx.DrawLine(pen, 375, cardTopOffset + 130, 375 + 100, cardTopOffset + 90);

            formatter.DrawString(_votingVenue, smallF, XBrushes.DarkGray, venueBox, XStringFormats.TopLeft);
            formatter.DrawString(_sender, smallF, XBrushes.DarkGray, senderBox, XStringFormats.TopLeft);
            formatter.DrawString(_receiver, smallF, XBrushes.DarkGray, recieverBox, XStringFormats.TopLeft);

            formatter.DrawString(_electionType +
                "\n" + _electionDate.ToShortDateString(),
                titleF, XBrushes.Black, electionBox, XStringFormats.TopLeft);
            formatter.DrawString(venue.Name + "\n"
                + venue.Address + "\n"
                + venue.PostalCode + " " + venue.PostalDistrict,
                boldF, XBrushes.Black, venueTextBox, XStringFormats.TopLeft);
            formatter.DrawString(_votingNumber + ": " + person.VotercardNumber, boldF, XBrushes.Black, voterNumberTextBox);
            formatter.DrawString(_votingTime + ":  " + _votingTimestamp, normalF, XBrushes.Black, timesTextBox);
            formatter.DrawString(person.FirstName + " "
                + person.LastName + "\n"
                + person.Address + "\n"
                + person.City + "\n"
                + person.PostalCode + "  "
                + person.PostalDistrict, normalF, XBrushes.Black, recieverTextBox);

            var postmark = XImage.FromFile(_postMark);
            gfx.DrawImage(postmark, _page.Width - 80, cardTopOffset + 5, postmark.PixelHeight, postmark.PixelWidth);

            gfx.Dispose();
            _voterCardsOnPage++;
        }

        public void SaveDocument(String filename = "PDFDocument.pdf") {
            try {
                _document.Save(filename);
            } catch (DirectoryNotFoundException) {
                Directory.CreateDirectory("print");
                _document.Save(filename);
            }
        }

        private bool IsPageFull() {
            bool res = _voterCardsOnPage % 4 == 0;
            if (res) _voterCardsOnPage = 0;
            return res;
        }

        private void CreateNewPage() {
            Contract.Requires(_document != null);
            _page = _document.AddPage();
        }
    }
}

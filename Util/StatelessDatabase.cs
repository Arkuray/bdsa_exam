﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Diagnostics.Contracts;

namespace Util.Client {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    public static class StatelessDatabase {
        private static readonly DatabaseAPI db
            = DatabaseAPI.GetInstance(ConfigurationManager.ConnectionStrings["VoterDatabase"].ConnectionString);
        [Pure]
        public static VenueData GetVenue() {
            string venueID = ConfigurationManager.AppSettings["VenueID"];
            var sqlQuery = "SELECT Name, Address, Postal, PostalDistrict FROM VotingVenue WHERE VotingVenueID = " + venueID;
            var reader = db.ExecuteReader(sqlQuery);
            return ReadVenueData(reader);
        }
        [Pure]
        public static VenueData GetVenue(uint voter) {
            var sqlQuery = "SELECT v.Name, v.Address ,v.Postal, v.PostalDistrict FROM VotingVenue v, VenueLookup lu WHERE v.VotingVenueID = lu.VotingVenueID AND lu.VoterID =" + voter;
            var reader = db.ExecuteReader(sqlQuery);
            return ReadVenueData(reader);
        }
        [Pure]
        private static VenueData ReadVenueData(DbDataReader reader) {
            Contract.Requires(reader != null);
            var result = new VenueData();
            if (!reader.HasRows) return result;
            reader.Read();
            result.Name = reader.GetString(0);
            result.Address = reader.GetString(1);
            result.PostalCode = (uint) reader.GetInt64(2);
            result.PostalDistrict = reader.GetString(3);
            return result;
        }
        [Pure]
        public static Boolean Exists(uint voter) {
            Contract.Ensures(Contract.Result<bool>() ? !GetPersonData(voter).Empty : GetPersonData(voter).Empty);
            return !GetPersonData(voter).Empty;
        }
        [Pure]
        public static PersonData GetPersonData(uint voter) {
            var sqlQuery = "SELECT * FROM Voter WHERE VoterID =" + voter;
            var reader = db.ExecuteReader(sqlQuery);
            return ReadPersonData(reader);
        }
        [Pure]
        private static PersonData ReadPersonData(DbDataReader reader) {
            Contract.Requires(reader != null);
            var result = new PersonData();
            if (!reader.Read()) return result;
            result.VotercardNumber = (uint) reader.GetInt64(0);
            result.City = reader.GetString(1);
            result.Address = reader.GetString(2);
            result.PostalCode = (uint) reader.GetInt64(3);
            result.PostalDistrict = reader.GetString(4);
            result.FirstName = reader.GetString(6);
            result.LastName = reader.GetString(7);
            return result;
        }
        [Pure]
        public static uint GetVoterID(String CPR) {
            var sqlQuery = "SELECT VoterID FROM Voter WHERE CPR =" + CPR;
            var result = db.ExecuteScalar(sqlQuery);
            if (result == null) throw new ArgumentException("CPR: " + CPR + " , was not found!");
            string str = ((long)result).ToString();
            return uint.Parse(str);
        }
        [Pure]
        public static IEnumerable<PersonData> GetAllPersonData() {
            //See project overview -- known bugs.
            //var sqlQuery = "SELECT * FROM Voter";
            var sqlQuery = "SELECT * FROM Voter LIMIT 100";
            var reader = db.ExecuteReader(sqlQuery);
            PersonData result;
            do {
                result = ReadPersonData(reader);
                yield return result;
            } while (!result.Empty);
        }
    }
}

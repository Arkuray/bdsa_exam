﻿using System;
using System.Collections.Generic;
using Util;


namespace Database_Creator {
    /// <author>
    /// Nikolaj Falsted
    /// nifa@itu.dk
    /// </author>
    class Program {
        static DatabaseAPI db = DatabaseAPI.GetInstance("Data Source=|DataDirectory|/../../../lib/VoterDatabase.s3db;");
        static void Main(string[] args) {
            ResetRegistration();
        }
        static int SetAllVoted() {
            var sqlQuery = "UPDATE Voter SET Voted = 1 WHERE Voted = 0";
            return db.ExecuteNonQuery(sqlQuery);
        }

        static int ResetRegistration() {
            var sqlQuery = "UPDATE Voter SET Voted = 0 WHERE Voted = 1";
            return db.ExecuteNonQuery(sqlQuery);
        }
        static int PopulateVenueRelation() {
            var sqlQuery = "SELECT VoterID, CPR FROM Voter";
            var reader = db.ExecuteReader(sqlQuery);
            var paramList = new List<List<string>> {new List<string>(29000), new List<string>(29000)};
            while (reader.Read()) {
                paramList[0].Add(reader.GetInt64(0)+"");
                paramList[1].Add(reader.GetString(1));
            }
            sqlQuery = "INSERT INTO VenueLookup(VoterID,CPR,VotingVenueID) VALUES(?,?,5)";
            return db.ExecuteNonQueryTransaction(sqlQuery, paramList);
        }

        static int CPRGenerator() {
            var stdDate = new DateTime(1900, 1, 1);
            var paramList = new List<List<string>> {new List<string>(29000), new List<string>(29000)};
            var rng = new Random();
            var sqlQuery = "SELECT VoterID FROM Voter";
            var reader = db.ExecuteReader(sqlQuery);
            while (reader.Read()) {
                var voterID = reader.GetInt64(0);
                var DoB = stdDate.AddDays(voterID * 6);
                var DoBArr = DoB.ToShortDateString().Split('-');
                string year = DoBArr[2].Substring(2);
                string CPR = DoBArr[0] + DoBArr[1] + year + rng.Next(1000, 9999);
                Console.WriteLine(CPR);
                paramList[0].Add(CPR);
                paramList[1].Add(voterID+"");
            }
            sqlQuery = "UPDATE Voter SET CPR = ? WHERE VoterID = ?";
            return db.ExecuteNonQueryTransaction(sqlQuery, paramList);
        }
        static int SplitNames() {
            var sqlQuery = "SELECT FullName, VoterID FROM Voter";
            var reader = db.ExecuteReader(sqlQuery);
            var paramList = new List<List<string>> { new List<string>(29000), new List<string>(29000), new List<string>(29000) };
            while (reader.Read()) {
                var fullName = reader.GetString(0);
                var nameArr = fullName.Split(' ');
                var lastName = nameArr[nameArr.Length - 1];
                var firstName = fullName.Remove(fullName.Length - lastName.Length);
                paramList[0].Add(firstName);
                paramList[1].Add(lastName);
                paramList[2].Add(reader.GetInt64(1)+"");
            }
            sqlQuery = "UPDATE Voter SET FirstName = ?, LastName = ? WHERE VoterID = ?";
            return db.ExecuteNonQueryTransaction(sqlQuery, paramList);
        }

        static void AddressData() {
            Console.WriteLine(InsertLivingPlace("Sunset Boulevard", "Los Angeles", "90027", "Hollywood", 1002, 2900));
            Console.WriteLine(InsertLivingPlace("Beverly Boulevard", "Los Angeles", "90021", "Hollywood", 1003, 2900));
            Console.WriteLine(InsertLivingPlace("Melrose Avenue", "Los Angeles", "90021", "Hollywood", 1004, 2900));
            Console.WriteLine(InsertLivingPlace("Clinton Street", "Los Angeles", "90024", "Hollywood", 1005, 2900));
            Console.WriteLine(InsertLivingPlace("Rosewood Avenue", "Los Angeles", "90022", "Hollywood", 1006, 2900));
            Console.WriteLine(InsertLivingPlace("Santa Monica Boulevard", "Los Angeles", "90024", "Hollywood", 1007, 2900));
            Console.WriteLine(InsertLivingPlace("Clinton Street", "Los Angeles", "90024", "Hollywood", 1008, 2920));
            Console.WriteLine(InsertLivingPlace("Wilshire Boulevard", "Los Angeles", "90025", "Hollywood", 1009, 2900));
            Console.WriteLine(InsertLivingPlace("Burton Way", "Los Angeles", "90029", "Hollywood", 1010, 2900));
            Console.WriteLine(InsertLivingPlace("Gregory Way", "Los Angeles", "90020", "Hollywood", 1011, 2900));
            Console.WriteLine(InsertLivingPlace("Durant Drive", "Los Angeles", "90023", "Hollywood", 1012, 2900));
        }
        static int InsertLivingPlace(string address, string city, string postalCode, string postalDistrict, int idStart, int lenght) {
            var paramList = new List<List<string>> {new List<string>(lenght), new List<string>(lenght)};
            var rng = new Random();
           
            for (uint n = 0; n < lenght*10; n+=10) {
                int randomStreetNumber = (int)(n/10) + rng.Next(0, 10);
                paramList[0].Add(address + " " + randomStreetNumber);
                paramList[1].Add((idStart+n) + "");
            }
            var sqlQuery = "UPDATE Voter SET Address = ?, City = '"+city+"', PostalCode = '"+postalCode+"', PostalDistrict = '"+postalDistrict+"' WHERE VoterID = ?";
            return db.ExecuteNonQueryTransaction(sqlQuery,paramList);
        }
    }
}

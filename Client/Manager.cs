﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Windows.Forms;
using Client.Display;
using Client.State;
using System.Threading;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Util;
using Util.Client;

namespace Client.Logic {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public class Manager {
        private readonly string _electionType;
        private readonly DateTime _electionDate;

        private Network _network;
        private GUI _ui;
        private VotercardScanner _scanner;

        private PlatformSetup _currentSetup;
        private uint _currentVoter = 0;

        private Process _server;
        
        public Manager(GUI ui) {
            _electionType = ConfigurationManager.AppSettings["ElectionType"];
            _electionDate = DateTime.Parse(ConfigurationManager.AppSettings["ElectionDate"]);

            _ui = ui;
            _ui.InitVenue(StatelessDatabase.GetVenue());
            _network = new Network();

            _ui.VoterEntered += GetPersonData;
            _ui.VoterRegistering += RegisterCurrentVoter;
            _ui.PlatformSetupChosen += SwapSetup;
            _ui.PrintPreviewing += PrintPreview;
            _ui.PrintingSingle += PrintSingle;
            _ui.PrintingAll += PrintAll;
        }

        private void FindQRCode() {
            while (true) {
                _ui.SetImageViewer(_scanner.ScanCamera());
                Thread.Sleep(300);
            }
        }

        private void GetPersonData(uint voter) {
            if (_currentVoter != voter) {
                var result = StatelessDatabase.GetPersonData(voter);
                if (!result.Empty) {
                    _currentVoter = voter;
                    UpdateUI(_ui.ShowPersonData, result);
                } else { //if no person data, try to find venue
                    var venue = StatelessDatabase.GetVenue(voter);
                    if (!venue.Empty) //show venue if found
                        UpdateUI(_ui.ShowWrongVenue, venue.Name);
                }
            }
        }

        private void RegisterCurrentVoter() {
            //Contract.Requires(_currentVoter != 0);
            try {
                if (_network.RegisterVoter(_currentVoter))
                    UpdateUI(_ui.ShowSuccesfulRegistration);
                else
                    UpdateUI(_ui.ShowAlreadyRegistred);
            } catch (System.TimeoutException e) {
                UpdateUI(_ui.ShowTimeout);
            }
        }

        private void SwapSetup(PlatformSetup setup) {
            switch (_currentSetup = setup) {
                case PlatformSetup.Combi:
                    if (_scanner == null) StartScanner();
                    if (_server == null) StartServer();
                    _ui.ClientTabEnabled = true;
                    _ui.ServerTabEnabled = true;
                    break;
                case PlatformSetup.Server:
                    if (_server == null) StartServer();
                    _ui.ClientTabEnabled = false;
                    _ui.ServerTabEnabled = true;
                    break;
                case PlatformSetup.Client:
                    if (_scanner == null) StartScanner();
                    if (_server != null) {
                        _server.Kill();
                        _server = null;
                    }
                    _ui.ClientTabEnabled = true;
                    _ui.ServerTabEnabled = false;
                    break;
            }
        }

        private void StartScanner() {
            _scanner = new VotercardScanner();
            if (_scanner.IsCameraAvaiable()) {
                _scanner.QRCodeFound += GetPersonData;
                new Action(FindQRCode).BeginInvoke(null, null);
            }
        }

        private void StartServer() {
            _server = new Process();
            _server.StartInfo = new ProcessStartInfo("Server.exe");
            _server.StartInfo.UseShellExecute = false;
            if (ConfigurationManager.AppSettings["NoServerWindow"].Equals("true"))
                _server.StartInfo.CreateNoWindow = true;
            _server.StartInfo.RedirectStandardOutput = true;
            _server.StartInfo.RedirectStandardError = true;
            _server.EnableRaisingEvents = true;
            _server.OutputDataReceived += Server_OutputDataReceived;
            _server.ErrorDataReceived += Server_OutputDataReceived;
            _server.Exited += Server_Exited;
            _server.Start();
            _server.BeginOutputReadLine();
            _server.BeginErrorReadLine();
        }

        private void Server_OutputDataReceived(object sender, DataReceivedEventArgs args) {
            if (args.Data != null)
                UpdateUI<string>(_ui.ShowServerOutput, args.Data);
        }

        private void Server_Exited(object sender, EventArgs args) {
            _server = null;
            if (_currentSetup != PlatformSetup.Client)
                UpdateUI(_ui.ShowServerDead);
        }

        private void PrintPreview(string cpr) {
            try {
                uint id = StatelessDatabase.GetVoterID(cpr);
                PersonData person = StatelessDatabase.GetPersonData(id);
                VenueData venue = StatelessDatabase.GetVenue(id);
                _ui.ShowPreview(id, person, venue);
            } catch (ArgumentException e) {
            }
        }

        private void PrintSingle(string cpr) {
            try {
                uint id = StatelessDatabase.GetVoterID(cpr);
                PersonData person = StatelessDatabase.GetPersonData(id);
                VenueData venue = StatelessDatabase.GetVenue(id);
                VotercardBuilder cardbuilder = new VotercardBuilder();
                cardbuilder.SetElectionType(_electionType);
                cardbuilder.SetElectionDate(_electionDate);
                cardbuilder.CreateNewDocument("Votercard " + id);
                cardbuilder.WriteVoterCard(person, venue);
                cardbuilder.SaveDocument("print/" + id + ".pdf");
                Process.Start("print\\" + id + ".pdf");
            } catch (ArgumentException e) {
            }
        }

        private void PrintAll() {
            VotercardBuilder cardbuilder = new VotercardBuilder();
            cardbuilder.SetElectionType(_electionType);
            cardbuilder.SetElectionDate(_electionDate);
            cardbuilder.CreateNewDocument("Votercards all");
            VenueData venue = StatelessDatabase.GetVenue();
            foreach (PersonData person in StatelessDatabase.GetAllPersonData()) {
                cardbuilder.WriteVoterCard(person, venue);
                Console.WriteLine("PRINTED CARD "+person.FirstName);
            }
            cardbuilder.SaveDocument("print/all.pdf");
            Process.Start("print\\all.pdf");
        }

        private void UpdateUI(Action update) {
            _ui.Invoke(update, null);
        }

        private void UpdateUI<Param>(Action<Param> update, Param param) {
            _ui.Invoke(update, new object[] { param });
        }

        ~Manager() {
            if (_server != null)
                _server.Kill();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Client {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public partial class TimeoutDialog : Form {
        private TimeoutDialogResult _result = TimeoutDialogResult.Cancel;

        public TimeoutDialog() {
            InitializeComponent();
        }

        public new TimeoutDialogResult ShowDialog() {
            base.ShowDialog();
            return _result;
        }

        private void button_Reconnect_Click(object sender, EventArgs e) {
            _result = TimeoutDialogResult.Reconnect;
            Close();
        }

        private void button_StartServer_Click(object sender, EventArgs e) {
            _result = TimeoutDialogResult.StartServer;
            Close();
        }

        public enum TimeoutDialogResult {
            Reconnect,
            StartServer,
            Cancel
        }
    }
}

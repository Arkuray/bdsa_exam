﻿using System;
using System.Configuration;
using System.Resources;
using System.Windows.Forms;
using Emgu.CV;
using Util.Client;

namespace Client.Display {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public partial class GUI : Form {
        public delegate void VoterHandler(uint voter);
        public delegate void SetupHandler(PlatformSetup setup);
        public delegate void PrintHandler(string cpr);
        public delegate void EmptyHandler();

        public event VoterHandler VoterEntered;
        public event EmptyHandler VoterRegistering;
        public event SetupHandler PlatformSetupChosen;
        public event PrintHandler PrintPreviewing;
        public event PrintHandler PrintingSingle;
        public event EmptyHandler PrintingAll;

        public bool ClientTabEnabled { get; set; }
        public bool ServerTabEnabled { get; set; }

        private ResourceManager _resources;
        
        public GUI() {
            InitializeComponent();
            _resources = new ResourceManager("Client.Global", System.Reflection.Assembly.GetExecutingAssembly());

            Reset();
        }

        public void InitVenue(VenueData venue) {
            if (!venue.Empty) {
                label_Municipality.Text = venue.Name;
                label_Venue.Text = venue.Address;
            } else {
                label_Municipality.Text = _resources.GetString("NotFound");
                label_Venue.Text = _resources.GetString("NotFound");
            }
        }

        private void GUI_Load(object sender, EventArgs e) {
            SetupDialog dialog = new SetupDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
                PlatformSetupChosen(dialog.ChosenSetup);
            else
                Application.Exit();
            if (dialog.ChosenSetup == PlatformSetup.Server) tabControl1.SelectedIndex = 1;
        }

        public void SetImageViewer(Image<Emgu.CV.Structure.Gray, byte> image) {
            imageViewer.Image = image.ToBitmap(imageViewer.Width, imageViewer.Height);
        }

        public void ShowPersonData(PersonData voterData) {
            Reset();
            textBox_FirstName.Text = voterData.FirstName;
            textBox_LastName.Text = voterData.LastName;
            textBox_Address.Text = voterData.Address;
            textBox_City.Text = voterData.City;
            textBox_PostalCode.Text = voterData.PostalCode.ToString();
            textBox_PostalDistrict.Text = voterData.PostalDistrict;
            button_Register.Enabled = true;
        }

        public void ShowSuccesfulRegistration() {
            label_Success.Visible = true;
            textBox_VoterNumber.Enabled = true;
        }

        public void ShowAlreadyRegistred() {
            label_Failure.Visible = true;
            textBox_VoterNumber.Enabled = true;
        }

        public void ShowWrongVenue(string venue) {
            label_WrongVenue.Visible = true;
            textBox_VoterNumber.Enabled = true;
        }

        public void ShowTimeout() {
            switch (new TimeoutDialog().ShowDialog()) {
                case TimeoutDialog.TimeoutDialogResult.Reconnect:
                    VoterRegisteringAsynchronous();
                    break;
                case TimeoutDialog.TimeoutDialogResult.StartServer:
                    PlatformSetupChosen(PlatformSetup.Combi);
                    VoterRegisteringAsynchronous();
                    break;
                case TimeoutDialog.TimeoutDialogResult.Cancel:
                    button_Register.Enabled = true;
                    textBox_VoterNumber.Enabled = true;
                    break;
            }
        }

        public void ShowServerOutput(string output) {
            textBox_ServerOutput.AppendText(output + "\n");
        }

        public void ShowServerDead() {
            if (MessageBox.Show(_resources.GetString("ServerDeadDialogText"),
                _resources.GetString("ServerDeadDialogCaption"),
                MessageBoxButtons.OK) == DialogResult.OK) {
                PlatformSetupChosen(PlatformSetup.Client);
                tabControl1.SelectedIndex = 0;
            } else {
                Application.Exit();
            }
        }

        public void ShowPreview(uint id, PersonData voter, VenueData venue) {
            textBox_Preview.Clear();
            textBox_Preview.AppendText("Voter number: " + id + "\n");
            textBox_Preview.AppendText(voter.ToString());
            textBox_Preview.AppendText(venue.ToString());
        }

        private void Reset() {
            textBox_FirstName.Clear();
            textBox_LastName.Clear();
            textBox_Address.Clear();
            textBox_City.Clear();
            textBox_PostalCode.Clear();
            textBox_PostalDistrict.Clear();
            label_Success.Visible = false;
            label_Failure.Visible = false;
            label_WrongVenue.Visible = false;
            button_Register.Enabled = false;
        }

        private void textBox_VoterNumber_TextChanged(object sender, EventArgs e) {
            if (VoterEntered != null) {
                uint voter;
                if (uint.TryParse(textBox_VoterNumber.Text, out voter))
                        VoterEntered(voter);
            }
        }

        private void button_Register_Click(object sender, EventArgs e) {
            if (VoterRegistering != null) {
                button_Register.Enabled = false;
                textBox_VoterNumber.Enabled = false;
                VoterRegisteringAsynchronous();
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabControl1.SelectedIndex == 1) { // Server tab
                if (!ServerTabEnabled)
                    if (MessageBox.Show(_resources.GetString("StartServerDialogText"),
                        _resources.GetString("StartServerDialogCaption"),
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                        PlatformSetupChosen(PlatformSetup.Combi);
                    else
                        tabControl1.SelectedIndex = 0;
            } else if (tabControl1.SelectedIndex == 0) { // Client tab
                if (!ClientTabEnabled)
                    if (MessageBox.Show(_resources.GetString("StartClientDialogText"),
                        _resources.GetString("StartClientDialogCaption"),
                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                        PlatformSetupChosen(PlatformSetup.Combi);
                    else
                        tabControl1.SelectedIndex = 1;
            }
        }

        private void button_KillServer_Click(object sender, EventArgs e) {
            if (MessageBox.Show(_resources.GetString("KillServerDialogText"),
                _resources.GetString("KillServerDialogCaption"),
                MessageBoxButtons.OKCancel) == DialogResult.OK) {
                PlatformSetupChosen(PlatformSetup.Client);
                tabControl1.SelectedIndex = 0;
            }
        }

        private void VoterEnteredAsynchronous(uint voter) {
            VoterEntered.BeginInvoke(voter, null, null);
        }

        private void VoterRegisteringAsynchronous() {
            VoterRegistering.BeginInvoke(null, null);
        }

        private void button_PreviewSingle_Click(object sender, EventArgs e) {
            if (textBox_PrintSingle.Text != "")
                PrintPreviewing(textBox_PrintSingle.Text);
        }

        private void button_PrintSingle_Click(object sender, EventArgs e) {
            if (textBox_PrintSingle.Text != "")
                PrintingSingle(textBox_PrintSingle.Text);
        }

        private void button_PrintAll_Click(object sender, EventArgs e) {
            PrintingAll();
        }
    }
}

﻿namespace Client {
    partial class TimeoutDialog {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeoutDialog));
            this.button_Reconnect = new System.Windows.Forms.Button();
            this.button_StartServer = new System.Windows.Forms.Button();
            this.label_Timeout = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Reconnect
            // 
            resources.ApplyResources(this.button_Reconnect, "button_Reconnect");
            this.button_Reconnect.Name = "button_Reconnect";
            this.button_Reconnect.UseVisualStyleBackColor = true;
            this.button_Reconnect.Click += new System.EventHandler(this.button_Reconnect_Click);
            // 
            // button_StartServer
            // 
            resources.ApplyResources(this.button_StartServer, "button_StartServer");
            this.button_StartServer.Name = "button_StartServer";
            this.button_StartServer.UseVisualStyleBackColor = true;
            this.button_StartServer.Click += new System.EventHandler(this.button_StartServer_Click);
            // 
            // label_Timeout
            // 
            resources.ApplyResources(this.label_Timeout, "label_Timeout");
            this.label_Timeout.MaximumSize = new System.Drawing.Size(380, 0);
            this.label_Timeout.Name = "label_Timeout";
            // 
            // TimeoutDialog
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_Timeout);
            this.Controls.Add(this.button_StartServer);
            this.Controls.Add(this.button_Reconnect);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TimeoutDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Reconnect;
        private System.Windows.Forms.Button button_StartServer;
        private System.Windows.Forms.Label label_Timeout;
    }
}
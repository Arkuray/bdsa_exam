﻿using System;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Util;
using Util.Client;

namespace Client.State {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public class Network {
        private UdpClient _udp;
        private uint _msgid = 0;

        private readonly long _timeout; // Unit: Ticks (100 nanoseconds)
        private readonly int _sendfreq; // Unit: Milliseconds
        private readonly IPEndPoint _multiEP;

        public Network() {
            _timeout = long.Parse(ConfigurationManager.AppSettings["Timeout"]) * 10000;
            _sendfreq = int.Parse(ConfigurationManager.AppSettings["ResendFrequency"]);
            IPAddress multiIP = IPAddress.Parse(ConfigurationManager.AppSettings["MulticastIP"]);
            int multiPort = int.Parse(ConfigurationManager.AppSettings["MulticastPort"]);
            int clientPort = int.Parse(ConfigurationManager.AppSettings["ClientPort"]);

            _multiEP = new IPEndPoint(multiIP, multiPort);
            _udp = new UdpClient(clientPort);
            _udp.JoinMulticastGroup(multiIP);
        }

        public bool RegisterVoter(uint voter) {
            Contract.Requires(StatelessDatabase.Exists(voter));
            Message outgoing = new Message(Command.RegisterVoter, voter, _multiEP, _msgid++);
            try {
                return RequestRegistration(outgoing);
            } catch (TimeoutException e) {
                // No response was received:
                // Notify of suspected primary and try again.
                PrimaryNotResponding();
                return RequestRegistration(outgoing);
            }
        }

        private bool RequestRegistration(Message msg) {
            Message response;
            byte[] packet = msg.PreparePacket();
            long start = DateTime.Now.Ticks;

            while (DateTime.Now.Ticks < start + _timeout) {
                _udp.Send(packet, packet.Length, _multiEP);
                Thread.Sleep(_sendfreq);

                while (_udp.Available != 0) {
                    IPEndPoint endpoint = null;
                    byte[] incpack = _udp.Receive(ref endpoint);
                    response = Message.ExtractMessage(incpack, endpoint);
                    switch (response.Command) {
                        case Command.RegistrationDone:
                            return true;
                        case Command.AlreadyRegistered:
                            return false;
                    }
                }
            }

            // Request timed out. Second time this happens, this client considers itself disconnected.
            throw new TimeoutException("Connection to the network has been lost.");
        }

        private void PrimaryNotResponding() {
            Message msg = new Message(Command.PrimaryNotResponding, "", _multiEP);
            byte[] packet = msg.PreparePacket();
            _udp.Send(packet, packet.Length, _multiEP);
        }

        [ContractInvariantMethod]
        private void ObjectInvariant() {
            Contract.Invariant(_udp != null);
        }
    }
}

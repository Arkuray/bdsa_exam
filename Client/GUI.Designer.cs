﻿using System.Windows.Forms;

namespace Client.Display {
    partial class GUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.label_Municipality = new System.Windows.Forms.Label();
            this.label_Venue = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label_Success = new System.Windows.Forms.Label();
            this.label_Failure = new System.Windows.Forms.Label();
            this.label_WrongVenue = new System.Windows.Forms.Label();
            this.button_Register = new System.Windows.Forms.Button();
            this.label_PostalDistrict = new System.Windows.Forms.Label();
            this.textBox_PostalDistrict = new System.Windows.Forms.TextBox();
            this.label_PostalCode = new System.Windows.Forms.Label();
            this.textBox_PostalCode = new System.Windows.Forms.TextBox();
            this.textBox_City = new System.Windows.Forms.TextBox();
            this.label_City = new System.Windows.Forms.Label();
            this.label_Address = new System.Windows.Forms.Label();
            this.label_LastName = new System.Windows.Forms.Label();
            this.textBox_Address = new System.Windows.Forms.TextBox();
            this.textBox_FirstName = new System.Windows.Forms.TextBox();
            this.label_FirstName = new System.Windows.Forms.Label();
            this.textBox_LastName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_ScanTip = new System.Windows.Forms.Label();
            this.label_VoterNumber = new System.Windows.Forms.Label();
            this.textBox_VoterNumber = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button_KillServer = new System.Windows.Forms.Button();
            this.textBox_ServerOutput = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox_Preview = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label_PrintAll = new System.Windows.Forms.Label();
            this.button_PrintAll = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_PrintSingle = new System.Windows.Forms.Button();
            this.button_PreviewSingle = new System.Windows.Forms.Button();
            this.label_PrintSingle = new System.Windows.Forms.Label();
            this.textBox_PrintSingle = new System.Windows.Forms.TextBox();
            this.imageViewer = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Municipality
            // 
            resources.ApplyResources(this.label_Municipality, "label_Municipality");
            this.label_Municipality.Name = "label_Municipality";
            // 
            // label_Venue
            // 
            resources.ApplyResources(this.label_Venue, "label_Venue");
            this.label_Venue.Name = "label_Venue";
            // 
            // tabControl1
            // 
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.BackColor = System.Drawing.Color.Snow;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Name = "tabPage1";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.label_Success);
            this.groupBox2.Controls.Add(this.label_Failure);
            this.groupBox2.Controls.Add(this.label_WrongVenue);
            this.groupBox2.Controls.Add(this.button_Register);
            this.groupBox2.Controls.Add(this.label_PostalDistrict);
            this.groupBox2.Controls.Add(this.textBox_PostalDistrict);
            this.groupBox2.Controls.Add(this.label_PostalCode);
            this.groupBox2.Controls.Add(this.textBox_PostalCode);
            this.groupBox2.Controls.Add(this.textBox_City);
            this.groupBox2.Controls.Add(this.label_City);
            this.groupBox2.Controls.Add(this.label_Address);
            this.groupBox2.Controls.Add(this.label_LastName);
            this.groupBox2.Controls.Add(this.textBox_Address);
            this.groupBox2.Controls.Add(this.textBox_FirstName);
            this.groupBox2.Controls.Add(this.label_FirstName);
            this.groupBox2.Controls.Add(this.textBox_LastName);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // label_Success
            // 
            resources.ApplyResources(this.label_Success, "label_Success");
            this.label_Success.BackColor = System.Drawing.Color.Snow;
            this.label_Success.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Success.ForeColor = System.Drawing.Color.Green;
            this.label_Success.MaximumSize = new System.Drawing.Size(420, 70);
            this.label_Success.Name = "label_Success";
            // 
            // label_Failure
            // 
            resources.ApplyResources(this.label_Failure, "label_Failure");
            this.label_Failure.BackColor = System.Drawing.Color.Snow;
            this.label_Failure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Failure.ForeColor = System.Drawing.Color.Red;
            this.label_Failure.MaximumSize = new System.Drawing.Size(420, 70);
            this.label_Failure.Name = "label_Failure";
            // 
            // label_WrongVenue
            // 
            resources.ApplyResources(this.label_WrongVenue, "label_WrongVenue");
            this.label_WrongVenue.BackColor = System.Drawing.Color.Snow;
            this.label_WrongVenue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_WrongVenue.ForeColor = System.Drawing.Color.Orange;
            this.label_WrongVenue.MaximumSize = new System.Drawing.Size(420, 70);
            this.label_WrongVenue.Name = "label_WrongVenue";
            // 
            // button_Register
            // 
            resources.ApplyResources(this.button_Register, "button_Register");
            this.button_Register.Name = "button_Register";
            this.button_Register.UseVisualStyleBackColor = true;
            this.button_Register.Click += new System.EventHandler(this.button_Register_Click);
            // 
            // label_PostalDistrict
            // 
            resources.ApplyResources(this.label_PostalDistrict, "label_PostalDistrict");
            this.label_PostalDistrict.Name = "label_PostalDistrict";
            // 
            // textBox_PostalDistrict
            // 
            resources.ApplyResources(this.textBox_PostalDistrict, "textBox_PostalDistrict");
            this.textBox_PostalDistrict.Name = "textBox_PostalDistrict";
            // 
            // label_PostalCode
            // 
            resources.ApplyResources(this.label_PostalCode, "label_PostalCode");
            this.label_PostalCode.Name = "label_PostalCode";
            // 
            // textBox_PostalCode
            // 
            resources.ApplyResources(this.textBox_PostalCode, "textBox_PostalCode");
            this.textBox_PostalCode.Name = "textBox_PostalCode";
            // 
            // textBox_City
            // 
            resources.ApplyResources(this.textBox_City, "textBox_City");
            this.textBox_City.Name = "textBox_City";
            // 
            // label_City
            // 
            resources.ApplyResources(this.label_City, "label_City");
            this.label_City.Name = "label_City";
            // 
            // label_Address
            // 
            resources.ApplyResources(this.label_Address, "label_Address");
            this.label_Address.Name = "label_Address";
            // 
            // label_LastName
            // 
            resources.ApplyResources(this.label_LastName, "label_LastName");
            this.label_LastName.Name = "label_LastName";
            // 
            // textBox_Address
            // 
            resources.ApplyResources(this.textBox_Address, "textBox_Address");
            this.textBox_Address.Name = "textBox_Address";
            // 
            // textBox_FirstName
            // 
            resources.ApplyResources(this.textBox_FirstName, "textBox_FirstName");
            this.textBox_FirstName.Name = "textBox_FirstName";
            // 
            // label_FirstName
            // 
            resources.ApplyResources(this.label_FirstName, "label_FirstName");
            this.label_FirstName.Name = "label_FirstName";
            // 
            // textBox_LastName
            // 
            resources.ApplyResources(this.textBox_LastName, "textBox_LastName");
            this.textBox_LastName.Name = "textBox_LastName";
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.label_ScanTip);
            this.groupBox1.Controls.Add(this.label_VoterNumber);
            this.groupBox1.Controls.Add(this.textBox_VoterNumber);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label_ScanTip
            // 
            resources.ApplyResources(this.label_ScanTip, "label_ScanTip");
            this.label_ScanTip.MaximumSize = new System.Drawing.Size(200, 0);
            this.label_ScanTip.Name = "label_ScanTip";
            // 
            // label_VoterNumber
            // 
            resources.ApplyResources(this.label_VoterNumber, "label_VoterNumber");
            this.label_VoterNumber.MaximumSize = new System.Drawing.Size(250, 0);
            this.label_VoterNumber.Name = "label_VoterNumber";
            // 
            // textBox_VoterNumber
            // 
            resources.ApplyResources(this.textBox_VoterNumber, "textBox_VoterNumber");
            this.textBox_VoterNumber.Name = "textBox_VoterNumber";
            this.textBox_VoterNumber.TextChanged += new System.EventHandler(this.textBox_VoterNumber_TextChanged);
            // 
            // tabPage2
            // 
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.BackColor = System.Drawing.Color.Snow;
            this.tabPage2.Controls.Add(this.button_KillServer);
            this.tabPage2.Controls.Add(this.textBox_ServerOutput);
            this.tabPage2.Name = "tabPage2";
            // 
            // button_KillServer
            // 
            resources.ApplyResources(this.button_KillServer, "button_KillServer");
            this.button_KillServer.Name = "button_KillServer";
            this.button_KillServer.UseVisualStyleBackColor = true;
            this.button_KillServer.Click += new System.EventHandler(this.button_KillServer_Click);
            // 
            // textBox_ServerOutput
            // 
            resources.ApplyResources(this.textBox_ServerOutput, "textBox_ServerOutput");
            this.textBox_ServerOutput.Name = "textBox_ServerOutput";
            this.textBox_ServerOutput.ReadOnly = true;
            // 
            // tabPage4
            // 
            resources.ApplyResources(this.tabPage4, "tabPage4");
            this.tabPage4.BackColor = System.Drawing.Color.Snow;
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Name = "tabPage4";
            // 
            // groupBox5
            // 
            resources.ApplyResources(this.groupBox5, "groupBox5");
            this.groupBox5.Controls.Add(this.textBox_Preview);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.TabStop = false;
            // 
            // textBox_Preview
            // 
            resources.ApplyResources(this.textBox_Preview, "textBox_Preview");
            this.textBox_Preview.BackColor = System.Drawing.Color.Snow;
            this.textBox_Preview.Name = "textBox_Preview";
            this.textBox_Preview.ReadOnly = true;
            // 
            // groupBox4
            // 
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Controls.Add(this.label_PrintAll);
            this.groupBox4.Controls.Add(this.button_PrintAll);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // label_PrintAll
            // 
            resources.ApplyResources(this.label_PrintAll, "label_PrintAll");
            this.label_PrintAll.MaximumSize = new System.Drawing.Size(200, 0);
            this.label_PrintAll.Name = "label_PrintAll";
            // 
            // button_PrintAll
            // 
            resources.ApplyResources(this.button_PrintAll, "button_PrintAll");
            this.button_PrintAll.Name = "button_PrintAll";
            this.button_PrintAll.UseVisualStyleBackColor = true;
            this.button_PrintAll.Click += new System.EventHandler(this.button_PrintAll_Click);
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.button_PrintSingle);
            this.groupBox3.Controls.Add(this.button_PreviewSingle);
            this.groupBox3.Controls.Add(this.label_PrintSingle);
            this.groupBox3.Controls.Add(this.textBox_PrintSingle);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // button_PrintSingle
            // 
            resources.ApplyResources(this.button_PrintSingle, "button_PrintSingle");
            this.button_PrintSingle.Name = "button_PrintSingle";
            this.button_PrintSingle.UseVisualStyleBackColor = true;
            this.button_PrintSingle.Click += new System.EventHandler(this.button_PrintSingle_Click);
            // 
            // button_PreviewSingle
            // 
            resources.ApplyResources(this.button_PreviewSingle, "button_PreviewSingle");
            this.button_PreviewSingle.Name = "button_PreviewSingle";
            this.button_PreviewSingle.UseVisualStyleBackColor = true;
            this.button_PreviewSingle.Click += new System.EventHandler(this.button_PreviewSingle_Click);
            // 
            // label_PrintSingle
            // 
            resources.ApplyResources(this.label_PrintSingle, "label_PrintSingle");
            this.label_PrintSingle.Name = "label_PrintSingle";
            // 
            // textBox_PrintSingle
            // 
            resources.ApplyResources(this.textBox_PrintSingle, "textBox_PrintSingle");
            this.textBox_PrintSingle.Name = "textBox_PrintSingle";
            // 
            // imageViewer
            // 
            resources.ApplyResources(this.imageViewer, "imageViewer");
            this.imageViewer.Name = "imageViewer";
            this.imageViewer.TabStop = false;
            // 
            // GUI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.imageViewer);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label_Venue);
            this.Controls.Add(this.label_Municipality);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GUI";
            this.Load += new System.EventHandler(this.GUI_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Municipality;
        private System.Windows.Forms.Label label_Venue;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_ScanTip;
        private System.Windows.Forms.Label label_VoterNumber;
        private System.Windows.Forms.TextBox textBox_VoterNumber;
        private System.Windows.Forms.TextBox textBox_Address;
        private System.Windows.Forms.Label label_LastName;
        private System.Windows.Forms.TextBox textBox_LastName;
        private System.Windows.Forms.Label label_FirstName;
        private System.Windows.Forms.TextBox textBox_FirstName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_PostalDistrict;
        private System.Windows.Forms.TextBox textBox_PostalDistrict;
        private System.Windows.Forms.Label label_PostalCode;
        private System.Windows.Forms.TextBox textBox_PostalCode;
        private System.Windows.Forms.TextBox textBox_City;
        private System.Windows.Forms.Label label_City;
        private System.Windows.Forms.Label label_Address;
        private System.Windows.Forms.Label label_Success;
        private System.Windows.Forms.Button button_Register;
        private System.Windows.Forms.Label label_WrongVenue;
        private System.Windows.Forms.Label label_Failure;
        private System.Windows.Forms.TextBox textBox_ServerOutput;
        private System.Windows.Forms.Button button_KillServer;
        private TabPage tabPage4;
        private GroupBox groupBox4;
        private GroupBox groupBox3;
        private Label label_PrintSingle;
        private TextBox textBox_PrintSingle;
        private Button button_PreviewSingle;
        private GroupBox groupBox5;
        private Button button_PrintSingle;
        private Label label_PrintAll;
        private Button button_PrintAll;
        private PictureBox imageViewer;
        private TextBox textBox_Preview;





    }
}


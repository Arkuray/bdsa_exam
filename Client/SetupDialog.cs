﻿using System;
using System.Windows.Forms;
using Util.Client;

namespace Client {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    public partial class SetupDialog : Form {
        public PlatformSetup ChosenSetup { get; private set; }

        public SetupDialog() {
            InitializeComponent();
        }

        private void button_Client_Click(object sender, EventArgs e) {
            ChosenSetup = PlatformSetup.Client;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button_Combi_Click(object sender, EventArgs e) {
            ChosenSetup = PlatformSetup.Combi;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button_Server_Click(object sender, EventArgs e) {
            ChosenSetup = PlatformSetup.Server;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}

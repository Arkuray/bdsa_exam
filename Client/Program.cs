﻿using System;
using System.Windows.Forms;
using Client.Display;
using Client.Logic;

namespace Client {
    /// <author>
    /// Jacob Fischer
    /// jaco@itu.dk
    /// </author>
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GUI ui = new GUI();
            new Manager(ui);
            Application.Run(ui);
        }
    }
}

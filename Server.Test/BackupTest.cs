﻿using System;
using System.Collections.Generic;
using Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using Util;

namespace Server.Test2
{
    /// <summary>
    ///This is a test class for BackupTest and is intended
    ///to contain all BackupTest Unit Tests
    ///To run this suite it is required to have an empty database for each run.
    ///Use the Database_Creator to clear the database. Just run the main method.
    ///</summary>
    [TestClass()]
    public class BackupTest {


        private TestContext testContextInstance;
        private Backup_Accessor target;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize() {
            target = new Backup_Accessor();
        }
        
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup() {
            target.Disconnect();
            target._udp.Close();
        }

        /// <summary>
        ///A test for ProcessMessage
        /// BackupRegistration (no registration beforehand)
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageBackupRegistrationTest() {
            target._primaryEP = null;
            Message message = new Message(Command.BackupRegistration, (uint)1337, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234)); 
            Message expected = new Message(Command.BackupAcknowledged, (uint)1337, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234)); 
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);

            target._primaryEP = new IPEndPoint(IPAddress.Parse("192.168.1.10"), 3334);
            message = new Message(Command.BackupRegistration, (uint)1337, new IPEndPoint(IPAddress.Parse("192.168.1.11"), 3334));
            expected = new Message(Command.StepDown, (uint)1337, new IPEndPoint(IPAddress.Parse("192.168.1.11"), 3334));
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// MakeDatabase
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageMakeDatabaseTest() {
            Message message = new Message(Command.MakeDatabase, new List<uint> {1338, 1339, 1340}, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// PrimaryNotResponding
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageNotRespondingTest() {
            target._primaryEP = null; //This will be when backup is first created
            Message message = new Message(Command.PrimaryNotResponding, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message expected = (Message)null;
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.IsNull(actual);

            target._primaryEP = new IPEndPoint(IPAddress.Parse("192.168.1.10"), 3334); //This will be when the backup is part of the server group
            message = new Message(Command.PrimaryNotResponding, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            expected = new Message(Command.AreYouAlive, "", new IPEndPoint(IPAddress.Parse("192.168.1.10"), 3334));
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// PrimaryWasAlive
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageAmAliveTest() {
            Message message = new Message(Command.AmAlive, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message expected = new Message(Command.InterruptElection, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// Candidate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageCandidateTest() {
            Message message = new Message(Command.Candidate, "", new IPEndPoint(IPAddress.Parse("192.168.0.1"), 2232));
            Message expected = new Message(Command.Candidate, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// InteruptElection
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageInteruptElectionTest() {
            Message message = new Message(Command.InterruptElection, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.IsNull(actual);
        }
    }
}

﻿using System.Net;
using Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Util;

namespace Server.Test2
{
    
    
    /// <summary>
    ///This is a test class for PrimaryTest and is intended
    ///to contain all PrimaryTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PrimaryTest {


        private TestContext testContextInstance;
        private static Primary_Accessor target;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize() {
            target = new Primary_Accessor();
        }

        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup() {
            target.Disconnect();
            target._udp.Close();
        }

        /// <summary>
        ///A test for ProcessMessage
        /// RegisterVoter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageRegisterVoterTest() {
            Message message = new Message(Command.RegisterVoter, (uint)9001, new IPEndPoint(IPAddress.Parse("192.168.0.20"), 3355));
            Message expected = new Message(Command.BackupRegistration, (uint)9001, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);

            //Unable to spam the server, and as such this will return null, and abstract server will discard it in the loop.
            message = new Message(Command.RegisterVoter, (uint)9001, new IPEndPoint(IPAddress.Parse("192.168.0.20"), 3355));
            actual = target.ProcessMessage(message);
            Assert.IsNull(actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// RegisterVoter - Already voted
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageAlreadyVotedTest() {
            Message message = new Message(Command.RegisterVoter, (uint)9001, new IPEndPoint(IPAddress.Parse("192.168.0.20"), 3355));
            Message expected = new Message(Command.AlreadyRegistered, (uint)9001, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// BackupAcknowledged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageBackupAcknowledgedTest() {
            //Not knowing the backup. Afterward, the primary knows the backup
            Message message = new Message(Command.BackupAcknowledged, (uint)9001, new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355), 1);
            Message expected = new Message(Command.MakeDatabase, (uint)9001, new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355), 1);
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);

            //Start a new registration
            message = new Message(Command.RegisterVoter, (uint)9002, new IPEndPoint(IPAddress.Parse("192.168.0.20"), 3355), 2);
            expected = new Message(Command.BackupRegistration, (uint)9002, new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234), target.GetUniqueID(IPAddress.Parse("192.168.0.20"), 2));
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);

            //The backup can now complete the registation
            message = new Message(Command.BackupAcknowledged, (uint)9002, new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355), actual.ID);
            expected = new Message(Command.RegistrationDone, (uint)9002, new IPEndPoint(IPAddress.Parse("192.168.0.20"), 3355), actual.ID);
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// AreYouAlive
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageAreAliveTest() {
            Message message = new Message(Command.AreYouAlive, "", new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355));
            Message expected = new Message(Command.AmAlive, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// Candidate
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageCandidateTest() {
            Message message = new Message(Command.Candidate, "", new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355));
            Message expected = new Message(Command.InterruptElection, "", new IPEndPoint(IPAddress.Parse("230.0.0.1"), 1234));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// GiveMeSnapshot
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageGiveSnapshotTest() {
            Message message = new Message(Command.GiveMeSnapshot, "", new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355));
            Message expected = new Message(Command.MakeDatabase, "", new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ProcessMessage
        /// StepDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("Server.exe")]
        public void ProcessMessageStepDownTest() {
            Message message = new Message(Command.StepDown, "", new IPEndPoint(IPAddress.Parse("192.168.0.25"), 3355));
            Message actual;
            actual = target.ProcessMessage(message);
            Assert.IsNull(actual);
            Assert.IsTrue(target.IsRankChangable());
        }
    }
}

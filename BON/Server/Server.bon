cluster_chart SERVER
	class SERVER_PROGRAM description "The entry point of the server application."
	class ABSTRACT_SERVER description "Superclass defining the structure of the generalized server loop."
	class BACKUP description "A backup replication manager, handling a local database replica."
	class PRIMARY description "The primary replication manager, handling all the backups as well as a local replica."
end

static_diagram SERVER_DIAGRAM
component
	cluster SERVER
	deferred class ABSTRACT_SERVER
	feature {ABSTRACT_SERVER}
		deferred ProcessMessage : MESSAGE -> inc : MESSAGE
		ChangeRank
			ensure
				IsRankChangeable();
			end
		GetNextTimeoutId : NATURAL
		SetTimeout -> id : NATURAL -> timeInMillis : NATURAL -> receiver : IPAddress -> callback : FUNC[IPAddress, NATURAL, MESSAGE]
	feature
		IsRankChangable : BOOLEAN
		ReceiveMessages
		ProcessMessages
		SendMessages
		Disconnect
	end

	class BACKUP
	inherit ABSTRACT_SERVER
	feature
		effective ProcessMessage : MESSAGE -> inc : MESSAGE
			require
				inc /= Void;
			ensure
				inc.Command = COMMAND.BACKUP_REGISTRATION -> (Result.Command = COMMAND.BACKUP_ACKNOWLEDGED or Result.Command = COMMAND.STEP_DOWN); --This could mean that there is more than one primary
				inc.Command = COMMAND.MAKE_DATABASE -> Result = Void;
				inc.Command = COMMAND.PRIMARY_NOT_RESPONDING -> (Result = Void or Result.Command = COMMAND.ARE_YOU_ALIVE);
				inc.Command = COMMAND.I_AM_ALIVE -> Result.Command = COMMAND.INTERRUPT_ELECTION;
				inc.Command = COMMAND.CANDIDATE -> (Result = Void or Result.Command = COMMAND.CANDIDATE);
				inc.Command = COMMAND.INTERRUPT_ELECTION -> Result = Void;
			end
	end
	
	class PRIMARY
	inherit ABSTRACT_SERVER
	feature
		effective ProcessMessage : MESSAGE -> inc : MESSAGE 
			require
				inc /= Void;
			ensure
				inc.Command = COMMAND.REGISTER_VOTER -> (Result = Void or Result.Command = COMMAND.BACKUP_REGISTRATION or Result.Command = COMMAND.ALREADY_REGISTERED);
				inc.Command = COMMAND.BACKUP_ACKNOWLEDGED -> (Result = Void or Result.Command = COMMAND.REGISTRATION_DONE or Result.Command = COMMAND.MAKE_DATABASE);
				inc.Command = COMMAND.ARE_YOU_ALIVE -> Result.Command = COMMAND.I_AM_ALIVE;
				inc.Command = COMMAND.CANDIDATE -> Result.Command = COMMAND.INTERRUPT_ELECTION;
				inc.Command = COMMAND.GIVE_ME_SNAPSHOT -> Result.Command = COMMAND.MAKE_DATABASE;
			end 	
	end
	
	class SERVER_PROGRAM
	feature
		CurrentServer : ABSTRACT_SERVER
		ChangeServerRank : ABSTRACT_SERVER
			ensure
				CurrentServer = PRIMARY -> Result = BACKUP;
				CurrentServer = BACKUP -> Result = PRIMARY;
			end
		NoBackupsRequest : MESSAGE
	end
end

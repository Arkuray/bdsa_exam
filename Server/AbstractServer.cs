using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using Util;
using Util.Server;

namespace Server {
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
	abstract public class AbstractServer {
		private readonly Queue<Message> _incoming = new Queue<Message>();
		private readonly Queue<Message> _outgoing = new Queue<Message>();
        private readonly Dictionary<uint, Dictionary<IPAddress, Timeout>> _timeoutTable = new Dictionary<uint,Dictionary<IPAddress,Timeout>>();
	    private uint _id = 0;
	    private bool _changeRank = false;

        protected UdpClient _udp;
		protected readonly int _multiPort;
	    protected readonly IPAddress _multiIP;
		protected readonly IPEndPoint _multiEP;

		private readonly int TIMESLOT;

	    protected AbstractServer() {
            _multiIP = IPAddress.Parse(ConfigurationManager.AppSettings["MulticastIP"]);
            _multiPort = int.Parse(ConfigurationManager.AppSettings["MulticastPort"]);
			_multiEP = new IPEndPoint(_multiIP, _multiPort);
            _udp = new UdpClient();
            TIMESLOT = int.Parse(ConfigurationManager.AppSettings["Timeslot"]) * 10000;
		}

		abstract protected Message ProcessMessage(Message incoming);

        protected void ChangeRank() {
            Contract.Ensures(IsRankChangable());
            _changeRank = true;
        }

        protected void EnqueueMessage(Message message) {
            _outgoing.Enqueue(message);
        }

        protected uint GetNextTimeoutId () {
            return _id++;
        }

        protected void SetTimeout(uint id, int ms, IPAddress receiver, Func<IPAddress, uint, Message> callback) {
            if(!_timeoutTable.ContainsKey(id)) {
                _timeoutTable.Add(id, new Dictionary<IPAddress, Timeout>());
            }
            var entries = _timeoutTable[id];
            entries.Add(receiver, new Timeout(id, ms, callback));
        }

        private void ReleaseTimeout(uint id, IPAddress reciever) {
            if(_timeoutTable.ContainsKey(id)) {
                Dictionary<IPAddress, Timeout> entries;
                _timeoutTable.TryGetValue(id, out entries);
                entries.Remove(reciever);
            }
        }

        private void CheckTimeouts() {
            var blacklist = new List<KeyValuePair<uint, IPAddress>>();
            var whitelist = new List<KeyValuePair<Timeout, IPAddress>>();

            foreach (var tableCell in _timeoutTable) {
                uint id = tableCell.Key;
                var entries = tableCell.Value;
                foreach (var entry in entries) {
                    IPAddress reciver = entry.Key;
                    Timeout timeout = entry.Value;

                    if (timeout.IsTimedOut()) {
                        whitelist.Add(new KeyValuePair<Timeout, IPAddress>(timeout, reciver));
                        blacklist.Add(new KeyValuePair<uint, IPAddress>(id, reciver));
                    }
                }
            }

            foreach (var entry in blacklist) {
                ReleaseTimeout(entry.Key, entry.Value);
            }

            foreach (var entry in whitelist) {
                Message msg = entry.Key.Callback.Invoke(entry.Value, entry.Key.Id);
                if(msg != null) _outgoing.Enqueue(msg);
            }
        }
        

		public void ReceiveMessages() {
			long startTime = DateTime.Now.Ticks;
			IPEndPoint endPoint = null;

            CheckTimeouts();
			
			while(DateTime.Now.Ticks < startTime + TIMESLOT & _incoming.Count < 10) {
				if(_udp.Available != 0) {
					byte[] packet = _udp.Receive(ref endPoint);
                    try {
                        Message message = Message.ExtractMessage(packet, endPoint);

                        ReleaseTimeout(message.ID, endPoint.Address);

                        _incoming.Enqueue(message);
                    } catch (Exception e) {
                        Log.Append("Failed to decrypt message", EventType.From, endPoint.Address.ToString());
                    }
				} else {
					Thread.Sleep(10); //In order not to use 100% cpu power for our infinity loop
				}
			}
		}
		
		public void ProcessMessages() {
			long startTime = DateTime.Now.Ticks;
			
			while(DateTime.Now.Ticks < startTime + TIMESLOT & _incoming.Count > 0) {
				Message request = _incoming.Dequeue();
				Message response = ProcessMessage(request);
				if(response != null) _outgoing.Enqueue(response);
			}
			
		}

        public void SendMessages() {
			long startTime = DateTime.Now.Ticks;

		    while(DateTime.Now.Ticks < startTime + TIMESLOT & _outgoing.Count > 0) {
				Message message = _outgoing.Dequeue();
				IPEndPoint endPoint = message.Address;

				byte[] packet = message.PreparePacket();
				Log.AppendDebug("Message sent, lenght: " + packet.Length +"bytes",EventType.To,endPoint.Address.ToString());	
				_udp.Send(packet, packet.Length, endPoint);
			}
		}

        public Boolean IsRankChangable() {
            return _changeRank;
        }

        public void Disconnect() {
            if(_udp != null) _udp.Close();
        }
		
        private class Timeout {
            private int _timeoutInMilis { get; set; }
            private readonly long _initialTime = DateTime.Now.Ticks;
            public Func<IPAddress, uint, Message> Callback;
            public uint Id;
            public Timeout(uint id, int timeoutInMilis, Func<IPAddress, uint, Message> callback ) {
                Id = id;
                _timeoutInMilis = timeoutInMilis;
                Callback = callback;
            }

            [Pure]
            public Boolean IsTimedOut() {
                return _initialTime + _timeoutInMilis*10000 < DateTime.Now.Ticks;
            }
        }

        [ContractInvariantMethod]
        private void Invariant() {
            Contract.Invariant(_udp != null);
        }
	}
}


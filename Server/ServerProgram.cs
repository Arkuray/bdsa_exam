﻿using System.Diagnostics.Contracts;

namespace Server {
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
    class ServerProgram {
        private AbstractServer CurrentServer;

        public void RunProgram() {
            CurrentServer = new Backup();
            while (true) {
                while (!CurrentServer.IsRankChangable()) {
                    CurrentServer.ReceiveMessages();
                    CurrentServer.ProcessMessages();
                    CurrentServer.SendMessages();
                }
                CurrentServer.Disconnect();
                ChangeServerRank();
            }
        }

        private void ChangeServerRank() {
            Contract.Requires(CurrentServer.IsRankChangable());
            Contract.Ensures(Contract.OldValue(CurrentServer.GetType()) == typeof(Backup) ?
                CurrentServer.GetType() == typeof(Primary) :
                CurrentServer.GetType() == typeof(Backup));
            if (CurrentServer.GetType() == typeof(Backup)) {
                CurrentServer = new Primary();
            } else {
                CurrentServer = new Backup();
            }
        }

        public static void Main(string[] args) {
            var root = new ServerProgram();
            root.RunProgram();  
        }
    }
}

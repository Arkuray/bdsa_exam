﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using Util;
using Util.Server;

namespace Server{
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
    public class Primary : AbstractServer {
        private List<IPAddress> _backups = new List<IPAddress>();
        private Dictionary<IPEndPoint, uint> _clientUIDRelation = new Dictionary<IPEndPoint, uint>();
        private Dictionary<uint, List<IPAddress>> _respondingBackups = new Dictionary<uint, List<IPAddress>>();

        private readonly int _backupRegistrationTimeout;

        public Primary() {
            Log.Append("Starting server as primary", EventType.Event);
            _udp = new UdpClient(_multiPort);
            _udp.JoinMulticastGroup(_multiIP);
            _backupRegistrationTimeout = int.Parse(ConfigurationManager.AppSettings["BackupRegistrationTimeout"]);
        }

        override protected Message ProcessMessage(Message message) {
            Contract.Requires(message != null);
            Contract.Ensures(message.Command == Command.RegisterVoter ? Contract.Result<Message>() == null || Contract.Result<Message>().Command == Command.BackupRegistration | Contract.Result<Message>().Command == Command.AlreadyRegistered : true);
            Contract.Ensures(message.Command == Command.BackupAcknowledged ? Contract.Result<Message>() == null || Contract.Result<Message>().Command == Command.RegistrationDone | Contract.Result<Message>().Command == Command.MakeDatabase : true);
            Contract.Ensures(message.Command == Command.AreYouAlive ? Contract.Result<Message>().Command == Command.AmAlive : true);
            Contract.Ensures(message.Command == Command.Candidate ? Contract.Result<Message>().Command == Command.InterruptElection : true);
            Contract.Ensures(message.Command == Command.GiveMeSnapshot ? Contract.Result<Message>().Command == Command.MakeDatabase : true);
            
            Message result = null;
            switch (message.Command) {
                case Command.RegisterVoter:
                    result = RegisterVoter(message);
                    break;
                case Command.BackupAcknowledged:
                    result = RegistrationDone(message);
                    break;
                case Command.AreYouAlive:
                    result = IAmAlive(message.Address);
                    break;
                case Command.Candidate:
                    result = InteruptElection();
                    break;
                case Command.GiveMeSnapshot:
                    result = HandOutSnapshot(message.Address);
                    break;
                case Command.StepDown:
                    ChangeRank();
                    break;
            }

            return result;
        }

        private Message RegisterVoter(Message message) {
            uint voter = (uint) message.Content;
            IPEndPoint client = message.Address;
            Message response = null;
            uint uID = GetUniqueID(client.Address, message.ID);

            Log.AppendDebug("Request to backup voter: " + voter + " received, Current registration status = " + StatedependantDatabase.IsRegistered(voter), EventType.From, message.Address.Address.ToString());

            //Make sure the message is only processed once, even though it is recieved many times.
            if (_clientUIDRelation.ContainsKey(client) 
                && (_clientUIDRelation[client].Equals(uID) 
                || (_respondingBackups.ContainsKey(_clientUIDRelation[client]) 
                && _respondingBackups[_clientUIDRelation[client]].Count != 0))) {
                return null;
            }

            if (_clientUIDRelation.ContainsKey(client)) _clientUIDRelation[client] = uID;
            else _clientUIDRelation.Add(client, uID);

            if (StatedependantDatabase.IsRegistered(voter)) {
                Log.Append(voter + " was allready registred", EventType.To, message.Address.Address.ToString());
                response = new Message(Command.AlreadyRegistered, voter, client);

            } else {
                StatedependantDatabase.RegisterVoter(voter);
                StartRegistrationObserver(uID, client);
                response = new Message(Command.BackupRegistration, voter, _multiEP, uID);

                if(_backups.Count == 0) EnqueueMessage(new Message(Command.RegistrationDone, voter, client));
                Log.AppendDebug("Registration of " + voter + " was succesful", EventType.From, message.Address.Address.ToString());
                Log.AppendDebug("Requesting registration of " + voter, EventType.To, "Backups");
            }
            return response;
        }

        private Message RegistrationDone(Message message) {
            if (!_backups.Contains(message.Address.Address)) {
                Log.AppendDebug("Unknown backup detected, sending snapshot", EventType.To, message.Address.Address.ToString());
                return HandOutSnapshot(message.Address);
            }

            Log.Append("Recieved backup respond for " + message.Content, EventType.From, message.Address.Address.ToString());
            IPEndPoint client = ObserveRegistration(message.ID, message.Address.Address);

            if (client != null) {
                Log.Append(message.Content + " was succesfully registred", EventType.To, client.Address.ToString());
                return new Message(Command.RegistrationDone, message.Content, client);
            }
            return null;
        }

        private Message IAmAlive(IPEndPoint requester) {
            Log.AppendDebug("Sending vital signs", EventType.To, requester.Address.ToString());
            Message response = new Message(Command.AmAlive, "", requester);
            return response;
        }

        private Message InteruptElection() {
            Log.AppendDebug("Interrupting election", EventType.To, "Backups");
            return new Message(Command.InterruptElection, "", _multiEP);
        }

        private Message HandOutSnapshot(IPEndPoint requester) {
            Log.AppendDebug("Begining to send snapshot", EventType.To,requester.Address.ToString());
            _backups.Add(requester.Address);

            var partedSnapshot = StatedependantDatabase.GetRegisteredVoterDump();
            foreach (var partSnapshot in partedSnapshot.Where(v => v != partedSnapshot.Last())) {
                EnqueueMessage(new Message(Command.MakeDatabase, partSnapshot, requester));
                Log.AppendDebug("Snapshot with "+ partSnapshot.Count +" voterIDs", EventType.To, requester.Address.ToString());
            }
            Log.Append("Snapshot of all registered voters sent", EventType.To, requester.Address.ToString());
            return new Message(Command.MakeDatabase, partedSnapshot.Last(), requester);
        }

        //CALLBACKS
        private Message RegisterVoterCallback(IPAddress backup, uint uID) {
            Log.Append("Backup did not respond. Removing it from the pool", EventType.Event, backup.ToString());

            IPEndPoint client = ObserveRegistration(uID, backup);
            _backups.Remove(backup);

            return client == null ? null : new Message(Command.RegistrationDone, "", client);
        }

        //PRIVATE METHODS
        private void StartRegistrationObserver(uint uID, IPEndPoint client) {
            _respondingBackups.Add(uID, new List<IPAddress>(_backups));
            foreach (IPAddress backup in _backups) {
                SetTimeout(uID, _backupRegistrationTimeout, backup, RegisterVoterCallback);
            }
        }

        private IPEndPoint ObserveRegistration(uint uID, IPAddress backup) {
            var backupsNotResponded = _respondingBackups[uID];

            backupsNotResponded.Remove(backup);
            bool allBackupsDone = backupsNotResponded.Count == 0;
            return allBackupsDone ? FindClient(uID) : null;
        }

        private IPEndPoint FindClient(uint uID) {
            foreach (var u in _clientUIDRelation.Where(u => u.Value.Equals(uID))) {
                return u.Key;
            }
            return null;
        }

        private uint GetUniqueID(IPAddress client, uint id) {
            byte[] bAddress = client.GetAddressBytes();
            return (uint)(bAddress[bAddress.Length - 1] << 24 | id);
        }
    }
}

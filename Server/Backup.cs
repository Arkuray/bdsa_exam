﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Net.Sockets;
using System.Net;
using Util;
using Util.Server;

namespace Server{
    /// <author>
    /// Mathias Vielwerth
    /// mvie@itu.dk
    /// </author>
    public class Backup : AbstractServer {
        private IPEndPoint _primaryEP;
        private List<IPEndPoint> _candidateList = new List<IPEndPoint>();
        private Boolean ElectionInProgress = false;

        private readonly int _vitalSignsTimeout;
        private readonly int _electionTimeout;
        private readonly int _snapshotRequestTimeout;

        public Backup() {
            Log.Append("Starting server as backup", EventType.Event);

            _vitalSignsTimeout = int.Parse(ConfigurationManager.AppSettings["VitalSignsTimeout"]);
            _electionTimeout = int.Parse(ConfigurationManager.AppSettings["ElectionTimeout"]);
            _snapshotRequestTimeout = int.Parse(ConfigurationManager.AppSettings["SnapshotRequestTimeout"]);

            _udp = new UdpClient(_multiPort);
            _udp.JoinMulticastGroup(_multiIP);
            _udp.Client.ReceiveBufferSize = int.Parse(ConfigurationManager.AppSettings["ReceiveBufferSize"]);
            SendSnapshotRequest();
        }

        override protected Message ProcessMessage(Message message) {
            Contract.Requires(message != null);
            Contract.Ensures(!message.Command.Equals(Command.BackupRegistration) || (Contract.Result<Message>().Command.Equals(Command.BackupAcknowledged) || Contract.Result<Message>().Command.Equals(Command.StepDown)));
            Contract.Ensures(!message.Command.Equals(Command.MakeDatabase) || Contract.Result<Message>() == null);
            Contract.Ensures(!message.Command.Equals(Command.PrimaryNotResponding) || (Contract.Result<Message>() == null || Contract.Result<Message>().Command.Equals(Command.AreYouAlive)));
            Contract.Ensures(!message.Command.Equals(Command.AmAlive) || Contract.Result<Message>().Command.Equals(Command.InterruptElection));
            Contract.Ensures(!message.Command.Equals(Command.Candidate) || (Contract.Result<Message>() == null || Contract.Result<Message>().Command == Command.Candidate));
            Contract.Ensures(!message.Command.Equals(Command.InterruptElection) || Contract.Result<Message>() == null);
            Message result = null;
            switch (message.Command) {
                case Command.BackupRegistration:
                    result = BackupRegistration(message);
                    break;
                case Command.MakeDatabase:
                    MakeDatabase(message);
                    break;
                case Command.PrimaryNotResponding:
                    result = IsPrimaryAlive();
                    break;
                case Command.AmAlive:
                    result = PrimaryWasAlive();
                    break;
                case Command.Candidate:
                    result = Candidate(message.Address);
                    break;
                case Command.InterruptElection:
                    InteruptElection();
                    break;
            }

            return result;
        }

        private Message BackupRegistration(Message message) {
            uint voter = (uint)message.Content;
            Message response;

            Log.AppendDebug("Request to backup voter: " + voter + " received, Current registration status = " + StatedependantDatabase.IsRegistered(voter), EventType.From, message.Address.Address.ToString());

            if (_primaryEP != null && !message.Address.Equals(_primaryEP)) {
                response = new Message(Command.StepDown, voter, message.Address, message.ID);
            } else {
                StatedependantDatabase.RegisterVoter(voter);
                response = new Message(Command.BackupAcknowledged, voter, message.Address, message.ID);
                Log.Append("Request to register " + voter + " was succesful", EventType.From, message.Address.Address.ToString());
            }
            return response;
        }

        private void MakeDatabase(Message message) {
            Log.Append("Recieved database", EventType.From, "Primary");
            _primaryEP = message.Address;
            StatedependantDatabase.RegisterDump((List<uint>)message.Content);
        }

        private Message IsPrimaryAlive() {
            if (_primaryEP == null) return null;
            Log.AppendDebug("Requesting vital signs", EventType.To, "Primary");
            uint id = GetNextTimeoutId();
            Message response = new Message(Command.AreYouAlive, "", _primaryEP, id);
            SetTimeout(id, _vitalSignsTimeout, _primaryEP.Address, PrimaryDownCallback);
            return response;
        }

        private Message PrimaryWasAlive() {
            Log.AppendDebug("Received vital signs", EventType.From, "Primary");
            Log.AppendDebug("Interrupting Election", EventType.To, "Backups");
            return new Message(Command.InterruptElection, "", _multiEP);
        }

        private Message Candidate(IPEndPoint candidate) {
            Log.AppendDebug("Candidate appeal received", EventType.From, candidate.Address.ToString());
            Message result = null;
            if (!_candidateList.Contains(candidate)) { result = new Message(Command.Candidate, "", _multiEP);}
            _candidateList.Add(candidate);
            return result;
        }

        private void InteruptElection() {
            Log.AppendDebug("Election was interrupted", EventType.Event);
            ElectionInProgress = false;
        }

        //CALLBACKS
        private Message PrimaryDownCallback(IPAddress primary, uint id) {
            Log.AppendDebug("No primary seems to be alive", EventType.Event);
            Log.Append("Starting Election for a new primary", EventType.Event);
            SetTimeout(GetNextTimeoutId(), _electionTimeout, IPAddress.Any, ElectionOverCallback);
            ElectionInProgress = true;
            return new Message(Command.Candidate, "", _multiEP);
        }

        private Message ElectionOverCallback(IPAddress multiAll, uint id) {
            Log.AppendDebug("Election timeout reached", EventType.Event);

            if(ElectionInProgress) {
                _primaryEP = FindPrimaryEP();
                Log.Append(_primaryEP.Address + " won the election!",EventType.Event);
                if (_primaryEP.Address.Equals(GetLocalIP())) {
                    Log.Append("Promoted to primary!", EventType.Event);
                    ChangeRank();
                }
            }
            _candidateList = new List<IPEndPoint>();
            return null;
        }

        private Message StartupCallback(IPAddress ipAny, uint id) {
            return _primaryEP == null ? PrimaryDownCallback(IPAddress.Any, id) : null;
        }

        //PRIVATE METHODS
        private IPEndPoint FindPrimaryEP() {
            IPEndPoint res = null; //Using _multiEP, as it is on the highest namespace of the switch.
            long p = long.MaxValue;
            foreach (var candidate in _candidateList) {
                res = p <= candidate.Address.Address ? res : candidate;
                p = res.Address.Address;
            }
            return res;
        }

        private IPAddress GetLocalIP() {
            IPHostEntry host;
            IPAddress localIP = null;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList) {
                if (ip.AddressFamily.Equals(AddressFamily.InterNetwork)) {
                    localIP = ip;
                }
            }
            return localIP;
        } 

        private void SendSnapshotRequest() {
            uint id = GetNextTimeoutId();
            Message snapshotRequest = new Message(Command.GiveMeSnapshot, "", _multiEP, id);
            byte[] packet = snapshotRequest.PreparePacket();

            Log.Append("Requesting snapshot", EventType.To, "Primary");
            _udp.Send(packet, packet.Length, _multiEP);
            SetTimeout(id, _snapshotRequestTimeout, IPAddress.Any, StartupCallback);
        }
    }
}
